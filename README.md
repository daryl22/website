# REPARTITION DU TRAVAIL
------
Chacun des membres du groupe aura une section bien définie comme suit :
1. `Accueil | Roland`
2. `A propos de nous | Henri`
3. `services proposes | Daryl`
4. `Réalisations | Daniel`
5. `L'équipe | Arthur`
6. `Contacts | Bagi`

# NB: Les onglets suivants seront respectivements remplaces par leurs correspondants
HOME -> Accueil
About us -> A propos de nous
Services -> Services proposes
Portfolio -> Réalisations
Team -> l'équipe
Contacts -> Contacts


# Livrables
------
Afin d'eviter les confusions, chacun des membres devra "commit" avec le prefixe suivant : ``"date_tache"(exemple: "git commit -m "5_04_2021_acceuil_termine" "``)
1. Le controle sera fait chaque 02 jours durant 1 semaine à compter du 02/04.
2. Le projet finira donc au plus tard le 11/04 avec hébergement.
3. Avant la finition du projet, des tests seront faits sur le responsive et la fonctionnalite cote back-end.

# Méthode de travail
-----
Agile : Les modifications et ajustements se feront au fure et a mesure de l'avancee du projet.




Bon vendredi Saint ! :-)

